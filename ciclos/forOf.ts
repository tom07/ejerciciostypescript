interface Usuario{
    nombre :string;
    apellido:string;
}

var listadoDeUsuarios: Array<Usuario> = [
    {
        nombre: 'Ana',
        apellido: 'Carcamo'
    },
    {
        nombre: 'Lupita',
        apellido:'Jaurez'
    },
    {
        nombre: 'Elmer',
        apellido: 'Martinez'
    }
]

for(let cantidadRepetir: number = 0; cantidadRepetir < listadoDeUsuarios.length; cantidadRepetir ++){
    console.log(listadoDeUsuarios[cantidadRepetir].apellido)
}


for(let usuario of listadoDeUsuarios){
    console.log('Hola Bienvenido: ', usuario.nombre);
    console.log('Nos gustas tenerte denuevo :', usuario.apellido)
}


