//Incio de la clase
var Alumno = /** @class */ (function () {
    function Alumno() {
        this.nombre = '';
        this.apellido = '';
        this.peso = 0;
    }
    Alumno.prototype.mostrarMensaje = function () {
        console.log("Hola", this.nombre + " " + this.apellido);
    };
    Alumno.prototype.asignar = function (nombreParametro, apellidoParametro, pesoParametro) {
        this.nombre = nombreParametro;
        this.apellido = apellidoParametro;
        this.peso = pesoParametro;
        this.mostrarMensaje();
    };
    return Alumno;
}());
//Fin de la clase
var alumno1 = new Alumno();
alumno1.asignar("Alejandro", "Paz", 100);
var alumno2 = new Alumno();
alumno2.asignar("Maria", "Agurcia", 150);
var alumno3 = new Alumno();
alumno3.asignar("Johana", "Agurcia", 150);
