//Incio de la clase
class Alumno{
    private nombre: string;
    private apellido: string;
    private peso: number;

    constructor()
    {
        this.nombre = '';
        this.apellido = '';
        this.peso = 0;
    }
   
    private   mostrarMensaje() : void
    {
        console.log("Hola", this.nombre + " " + this.apellido);
    }

    asignar(nombreParametro:string, apellidoParametro:string, pesoParametro:number){
        this.nombre = nombreParametro;
        this.apellido = apellidoParametro;
        this.peso = pesoParametro;
        this.mostrarMensaje();
    }
}
//Fin de la clase


var alumno1: Alumno = new Alumno();
alumno1.asignar("Alejandro", "Paz", 100);


var alumno2: Alumno = new Alumno();
alumno2.asignar("Maria", "Agurcia", 150);

var alumno3: Alumno = new Alumno();
alumno3.asignar("Johana", "Agurcia", 150);

