//== Para comparar operaciones  si son iguales
// != para comparar operaciones distintas
//&& nos devuelve un verdadero cuando ambas expresiones son verdaderas
// || nos devuelve un verdadero si cualquier expresión es verdadera
var esNuevo = false;
var deseaImprimir = true;
var juan = "juan";
var juan2 = "Juan";
var listaNumero1 = 100;
var listaNumero2 = 120;
var sonNombresIguales = (juan == juan2);
var aceptaDescuentos = (15 + 18 == 33);
var sonAmbosVerdaderos = sonNombresIguales && aceptaDescuentos;
var sonLasListasIguales = (listaNumero1 != listaNumero2);
console.log("Acepta Descuentos: ", aceptaDescuentos);
console.log("Son Nombres Iguales: ", sonNombresIguales);
console.log("Son Ambos verdaderos", sonAmbosVerdaderos);
console.log("Las listas son iguales", sonLasListasIguales);
var esAlgunaVerdadera = sonNombresIguales || aceptaDescuentos || sonAmbosVerdaderos || sonLasListasIguales;
console.log("Es alguna Verdadera: ", esAlgunaVerdadera);
var sonTodasVerdaderas = sonNombresIguales && aceptaDescuentos && sonAmbosVerdaderos && sonLasListasIguales;
console.log("Son todas verdaderas", sonTodasVerdaderas);
