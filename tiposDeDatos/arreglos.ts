interface Alumnos{
    nombre: string;
    edad: number;
}

var listadoDeNombres: Array<string> = ["Juana","Maria", "Ana", "Carmen"];

var listadoDeNumeros: Array<number> = [ 15,25,36,695,95]

var listadoDeAlumnos: Array<Alumnos> = [
    {
        nombre: "Maria",
        edad: 15
    },
    {
        nombre: "Ana",
        edad: 95
    },
    {
        nombre: "Johana",
        edad: 54
    }
]
console.log(listadoDeAlumnos[2].nombre)