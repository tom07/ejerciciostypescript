//== Para comparar operaciones  si son iguales
// != para comparar operaciones distintas
//&& nos devuelve un verdadero cuando ambas expresiones son verdaderas
// || nos devuelve un verdadero si cualquier expresión es verdadera

var esNuevo: boolean = false;
var deseaImprimir: boolean = true;

var juan = "juan";
var juan2 = "Juan";
var listaNumero1: number = 100;
var listaNumero2: number = 120;

var sonNombresIguales:boolean = (juan == juan2)
var aceptaDescuentos:boolean = (15 + 18 == 33)
var sonAmbosVerdaderos:boolean = sonNombresIguales && aceptaDescuentos
var sonLasListasIguales: boolean = (listaNumero1 != listaNumero2)


console.log("Acepta Descuentos: ", aceptaDescuentos)
console.log("Son Nombres Iguales: ", sonNombresIguales)
console.log("Son Ambos verdaderos",sonAmbosVerdaderos)
console.log("Las listas son iguales", sonLasListasIguales)

var esAlgunaVerdadera: boolean = sonNombresIguales || aceptaDescuentos || sonAmbosVerdaderos || sonLasListasIguales
console.log("Es alguna Verdadera: " , esAlgunaVerdadera)


var sonTodasVerdaderas: boolean = sonNombresIguales && aceptaDescuentos && sonAmbosVerdaderos && sonLasListasIguales

console.log("Son todas verdaderas", sonTodasVerdaderas)







